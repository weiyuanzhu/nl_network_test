package network;

import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;


class TaskWithResult implements Callable<String>{
	
	private int id = 0;
	
	public TaskWithResult(int id){
		this.id = id;
	}

	@Override
	public String call() throws Exception {
		TimeUnit.SECONDS.sleep(3);
		return "result is " + id;
	}
	
	
}


public class CallableTest {

	public static void main(String[] args) {
		String s = null;
		ExecutorService exec = Executors.newCachedThreadPool();
		
		Future fs = exec.submit(new TaskWithResult(9));
		
		try {
			System.out.println("start get");
			s = (String) fs.get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("fs.get() finished: " + s);
		
	}

	
	
	
	
}
