

/*
 * Copyright (c) 1995, 2013, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 

package network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class EchoClient {
    public static void main(String[] args) throws IOException {
        
        /*if (args.length != 2) {
            System.err.println(
                "Usage: java EchoClient <host name> <port number>");
            System.exit(1);
        }

        String hostName = args[0];*/
        
    	String ip = "192.168.0.241";
        int portNumber = 4567;

        try (
            Socket echoSocket = new Socket(ip, portNumber);
        		
        		
            PrintWriter out = new PrintWriter(echoSocket.getOutputStream(), false);
            InputStream in = echoSocket.getInputStream();
        		
            BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in))
        ) 
        {
        	System.out.println("Received buffer size:" + echoSocket.getReceiveBufferSize());
        	
        	int temp = 0;
        	List<Integer> rxBuffer = new ArrayList<Integer>();
        	
        	int[] buffer = {0x02,0xA0,0x32,0x29,0xD5,0x5A,0xA5};
        	char[] txBuffer = new char[7];
        	
        	for(int i =0; i<buffer.length;i++)
        	{
        		txBuffer[i] = (char) buffer[i];
        		System.out.println(txBuffer[i]);
        		
        	}
            //String userInput;
            /*while ((userInput = stdIn.readLine()) != null) {
                out.println(userInput);
                System.out.println("echo: " + in.readLine());
            }*/
        	
        	out.println(txBuffer);
        	out.flush();
        	
        	
        	while(in.available()>0)
        	{
        		temp = in.read();
        		rxBuffer.add(temp);
        		System.out.print(temp + " ");
        		
        	}
        	
        } catch (UnknownHostException e) {
            //System.err.println("Don't know about host " + hostName);
            System.exit(1);
        } catch (IOException e) {
            //System.err.println("Couldn't get I/O for the connection to " +
                //hostName);
            System.exit(1);
        } 
       
    }
}