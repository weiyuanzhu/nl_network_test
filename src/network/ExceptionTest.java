package network;

class SimpleException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4375513137227826765L;
	private String str;
	
	public SimpleException(String s)
	{
		this.str = s;
	}

	public String getStr() {
		return str;
	}

	public void setStr(String str) {
		this.str = str;
	}
	
	
}



public class ExceptionTest {
	
	static int count = 0;

	private static void f() throws SimpleException
	{
		throw new SimpleException("throw test");
	}
	
	private static void g() throws NullPointerException
	{
		throw new NullPointerException();
	}
	
	public static void main(String[] args) {

		try{
			
			g();
			System.out.println("---------------------------------");
			f();
			System.out.println("---------------------------------");
		}
		catch(SimpleException |NullPointerException e){
			for(StackTraceElement ste : e.getStackTrace())
			{
				System.out.println(ste.getMethodName());
			}
			
		}
		
		try{
			
			f();
			System.out.println("---------------------------------");
			
		}
		catch(SimpleException |NullPointerException e){
			for(StackTraceElement ste : e.getStackTrace())
			{
				System.out.println(ste.getMethodName());
			}
			
		}
		
		while(true)
		{
			try{
				if(count++ == 0)
				{
					throw new SimpleException("test");
					
				}
				System.out.println("NO Exception");
			}
			catch(SimpleException e)
			{
				System.out.println("Simple Exception");
			}
			finally{
				System.out.println("In finally clause");
				if(count == 2) break;
			}
			
		}
		
		System.out.println("after finally");
		
	}

}
