package network;

import java.io.*;
import java.net.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class NClient {
	
	static String ip = "192.168.0.200";
	static final int port = 500;
	
	private static Socket socket = null;
	private static PrintWriter out = null;
	private static InputStream in = null;
	
	
	public static final int MASTER_GET = 0xA0;
	public static final int MASTER_TOGGLE = 0xA1;
	private static final int GET_INIT = 0x21;
	private static final int GET_REPORT_EMPTY = 0x22; 
	private static final int GET_FACTORY_RESET = 0x23;
	private static final int GET_FLASH_RESET = 0x24;
	private static final int GET_REPORT = 0x25;
	private static final int GET_LIST = 0x26;
	private static final int UPDATE_LIST = 0x27;
	private static final int GET_DATE_TIME = 0x29;
	private static final int GET_SCHEDULE_TEST_STATUS = 0x30;
	private static final int GET_SCHEDULE_DATA = 0x31;
	private static final int GET_OVERALL_STATUS = 0x32;
	private static final int SET_SET_SERIAL_NUMBER = 0x8B;
	static final int UART_STOP_BIT_H = 0x5A;
	static final int UART_STOP_BIT_L = 0xA5;
	static final int UART_NEW_LINE_H = 0x0D;
	static final int UART_NEW_LINE_L = 0x0A;

	
	private static List <Integer> txBuffer = null;
	private static List <Integer> rxBuffer = null;
	
	public List<Integer> getTx()
	{
		List <Integer> txBuffer = new ArrayList<Integer>();
		
		
		return txBuffer;
	}
	
	private static char[] getCmd(int comm)
	{
				
		List <Integer> list = new ArrayList<Integer>();
		
		list.add(0x02);
		list.add(MASTER_GET);
		list.add(comm);
		int checksum = CRC.calcCRC(list, list.size());
		list.add(CRC.getUnsignedInt(checksum));
		list.add(CRC.getUnsignedInt(checksum >> 8));
		
		list.add(UART_STOP_BIT_H);
		list.add(UART_STOP_BIT_L);
		//list.add(UART_NEW_LINE_H);
		//list.add(UART_NEW_LINE_L);
		
		System.out.println(list);
		
		char[] command = new char[list.size()];
		for(int j=0; j<list.size();j++)		
		{
			command[j] = (char) list.get(j).intValue();
			
		}
		
		
		
		return command;
	}
	
	private static char[] toggleCmd(int address)
	{
				
		List <Integer> list = new ArrayList<Integer>();
		
		list.add(0x02);
		list.add(MASTER_TOGGLE);
		list.add(0x6F);
		list.add(address);
		int checksum = CRC.calcCRC(list, list.size());
		list.add(CRC.getUnsignedInt(checksum));
		list.add(CRC.getUnsignedInt(checksum >> 8));
		
		list.add(UART_STOP_BIT_H);
		list.add(UART_STOP_BIT_L);
		//list.add(UART_NEW_LINE_H);
		//list.add(UART_NEW_LINE_L);
		
		System.out.println(list);
		
		char[] command = new char[list.size()];
		for(int j=0; j<list.size();j++)		
		{
			command[j] = (char) list.get(j).intValue();
			
		}
		
		
		
		return command;
	}
	
	
	
	
	public static void main(String[] args) throws UnknownHostException, IOException, InterruptedException
	{
		if(args.length > 0) {
			System.out.println(args[0]);
			ip = args[0];
		}
		
		int bytes_received = 0;
		txBuffer = new ArrayList<Integer>();
		rxBuffer = new ArrayList<Integer>();
		
		char[] getOverAllStatus = new char[] {0x02,0xA0,0x32,0x29,0xD5,0x5A,0xA5}; 
		char[] getConfiguration = new char[] {0x02,0xa0,0x21,0x68,0x18,0x5a,0xa5};
		char[] ftTest = new char[] {0x02,0xA1,0x60,0x00,0x78,0x7E,0x5A,0xA5};
		char[] stopFtTest = new char[] {0x02,0xA1,0x60,0xA9,0xB8,0x5A,0xA5};
		char[] getPackageTest = new char[] {2, 165, 64, 15, 96, 0,0x5A,0xA5};
		char[] getList = new char[] {0x02,0xA0,0x26,0x29,0xD5,0x5A,0xA5};
		char[] getReport = new char[] {};
		char[] getRelayUnit = new char[] {0x02, 0xA0, 0x33, 0xe8,0x15,0x5A,0xA5};
		char[] setGTIN_SN = new char[] {0x02, 0xA2, 0x8B, 0x04, 0x9A, 0x2B, 0xA5, 0xF4, 0x67, 0x10, 0x00, 0x00, 0x02, 0x76, 0x26,0x5A,0xA5};
		char[] connectionCheck = null;
		
		Date date = new Date();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateString = formatter.format(date);
		
		connectionCheck = dateString.toCharArray();
		
		//String test = new String(getConfiguration,0,getConfiguration.length);
		//System.out.println("\n----:  " +  test);
		
		try {
			System.out.println("Start connection to " + ip + ": " + port);
			socket = new Socket(ip, port);
			System.out.println("Connected to " + socket.getInetAddress() + ": " + socket.getPort());
			//socket.setSoTimeout(3 * 1000);
			out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(),"ISO8859_1")),false);		
			in = socket.getInputStream();
			
			System.out.print("Tx: ");
			for(int i = 0; i < getOverAllStatus.length; i++)	{
				System.out.print(String.format("%02X", (byte)getOverAllStatus[i]) + " ");
			}
			System.out.println();
			
			out.println(getOverAllStatus);
			out.flush();
			//System.out.println("flush done.");
			TimeUnit.SECONDS.sleep(1);	
			System.out.print("Rx: ");
			
			int len = -1;
			byte[] rxBufferArray = new byte[1024];
//			while ((len = in.read(rxBufferArray)) != -1) {
//				System.out.print("Rx: ");
//				for (int i = 0; i < len; i ++) {
//					int a = rxBufferArray[i] & 0xFF;
//					System.out.print(String.format("%02X", (byte)a) + " ");
//				}
//				System.out.println(" end");
//			}
			
			
			
			
			while( !socket.isClosed() &&  in.available() > 0 ) {
				int data = in.read();
				rxBuffer.add(data);
				if((data == UART_NEW_LINE_L) && !rxBuffer.isEmpty() && 
        				(rxBuffer.get(rxBuffer.size() - 2)==UART_NEW_LINE_H) &&
        				(rxBuffer.get(rxBuffer.size() - 3)==UART_STOP_BIT_L) &&
        				(rxBuffer.get(rxBuffer.size() - 4)==UART_STOP_BIT_H)) 
				{
					bytes_received += rxBuffer.size();
					
					for(int i=0; i<rxBuffer.size();i++)	{
						System.out.print(String.format("%02X", rxBuffer.get(i)) + " ");
					}
					rxBuffer.clear();
					
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try{
				if(socket != null) {				
					out.close();
					in.close();
					socket.close();
					System.out.println("\nSocket closed");
				}
			} catch(IOException e2) {
				e2.printStackTrace();
				}
	    } 
			
//			//System.out.println("Received buffer size: " + socket.getReceiveBufferSize());
//			
//			out.println(getOverAllStatus);
//			out.flush();
//			
//			System.out.println("Input end.");
//			
////			TimeUnit.SECONDS.sleep(2);
//					
//			System.out.println("available: "+ in.available());
//			
//			//byte[] buffer = new byte[in.available()];
//			
//			while( !socket.isClosed() &&  in.available() > 0 )
//			{
//				int data = in.read();
//				rxBuffer.add(data);
//				if((data == UART_NEW_LINE_L) && !rxBuffer.isEmpty() && 
//        				(rxBuffer.get(rxBuffer.size() - 2)==UART_NEW_LINE_H) &&
//        				(rxBuffer.get(rxBuffer.size() - 3)==UART_STOP_BIT_L) &&
//        				(rxBuffer.get(rxBuffer.size() - 4)==UART_STOP_BIT_H)) 
//				{
//					bytes_received += rxBuffer.size();
//					System.out.println("Received total bytes: " + bytes_received);
//					for(int i=0; i<rxBuffer.size();i++)
//					{
//						//System.out.print(String.format("%02X", Integer.toHexString(rxBuffer.get(i)) + " "));
//						System.out.print(String.format("%02X", rxBuffer.get(i)) + " ");
//					}
//					System.out.println("size: " + rxBuffer.size());
//					rxBuffer.clear();
//					
//				}
//			}
//			
//			
//			/*while(in.available()>0)
//			{	
//					data = in.read();
//					rxBuffer.add(data);
//					//System.out.print(data + " ");
//			}*/
//				
//			
//			/*int count = 0;
//			while (count == 0) {
//				   count = in.available();
//				  }
//			byte[]rx = new byte[count];
//			
//			int readCount = 0; 
//			while (readCount < count) {
//				
//			   readCount += in.read(rx, readCount, count - readCount);
//			}*/
//
//			
//		
//			
//		
//			for(int j = 0; j < rxBuffer.size();j++)
//			{
//				
//				if(	rxBuffer.size() >0 &&
//						rxBuffer.get(j) == UART_NEW_LINE_L &&  
//								rxBuffer.get(j-1) == UART_NEW_LINE_H &&
//										rxBuffer.get(j-2) == UART_STOP_BIT_L &&
//												rxBuffer.get(j-3) == UART_STOP_BIT_H && 
//        			j!=rxBuffer.get(j))
//				{
//        			System.out.print("\n");
//				}
//				else System.out.printf("%d%c", rxBuffer.get(j), (j%1024 == 0) && (j !=0 ) ? '\n': ' ' );
//			}
//			
//			System.out.println("\n"+ "Received " + rxBuffer.size() + " bytes  ");
//			
//			/*for(int j = 0; j < rxBuffer.size(); j++)		
//			{
//				//System.out.printf("%d %c",txBuffer.get(j), (j == txBuffer.size()-1) ? '\n' : ' ' );
//				System.out.printf("%02X",txBuffer.get(j) );
//			}*/
//			
//			rxBuffer.clear();
//			
//		}
//			
//			
//		
//		catch (IOException e1) {
//			e1.printStackTrace();
//		} finally {
//			try{
//				if(socket != null) {				
//					out.close();
//					in.close();
//					socket.close();
//				}
//			} catch(IOException e2) {
//				e2.printStackTrace();
//				}
//	    } 
	}
}
