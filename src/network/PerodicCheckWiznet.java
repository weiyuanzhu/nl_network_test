package network;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class PerodicCheckWiznet {
	
	private static class Task implements Runnable{

		@Override
		public void run() {
			Socket socket = null;
			PrintWriter out = null;
			
			Date date = new Date();
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String dateString = formatter.format(date);
			char[] connectionCheck = dateString.toCharArray();
			
			try{	
				socket = new Socket("192.168.1.23", 500);
				System.out.println(dateString);
				System.out.println("Connect to " + socket.getInetAddress() + ": " + socket.getPort());
				//socket.setSoTimeout(3 * 1000);
				out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(),"ISO8859_1")),false);
				
				out.println(connectionCheck);
				out.flush();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				
				try {
					out.close();
					socket.close();
					System.out.println("socket closed");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static void main(String[] args) {
		ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);			//assign executorService
		scheduler.scheduleAtFixedRate(new Task(), 0, 60, TimeUnit.SECONDS);					//start scheduled task
	}

}
