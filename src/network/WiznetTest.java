package network;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;


public class WiznetTest {
	
	public static void main(String[] args){
		System.out.println("---wiznet find test---");
		
		String ip = "192.168.1.19";
        int port = 1461;
        
        Socket wiznetSocket = null;
		PrintWriter out = null;
		BufferedReader in = null;
        
        try {
			wiznetSocket = new Socket(ip, port);
			
			
			out = new PrintWriter(new OutputStreamWriter(
				new BufferedOutputStream(wiznetSocket.getOutputStream()), "ISO8859_1"));
			in = new BufferedReader(new InputStreamReader(wiznetSocket.getInputStream()));
	        
			out.print("FIND");
    		out.flush();
    		
			char[] bufferRx = new char[512];
			int[] buffer = new int[512];
			int i = 0;
			in.read(bufferRx);
			for(char b : bufferRx) {
				buffer[i++] = CRC.getUnsignedInt(b);
			}
			
			System.out.println(buffer);
			
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
